#!/usr/bin/python3
# coding: utf-8

"""Övning 3.2

* Skriv ett program som läser in ett antal rader text från terminalen med
  input() tills en blank rad matas in.

* Beräkna antalet förekomster av varje ord (med ”ord” menar vi här en följd av
  icke-blanka tecken) i texten.

* Utskriften ska vara sorterad så att de mest frekventa orden anges först.

  Exempel: om den inmatade texten är

    Ett och två, tre
    och tre och fyra.

  ska utskriften bli

    och 3
    tre 2
    Ett 1
    två, 1
    fyra. 1

"""

count = {}
while True:
    L = input().split()
    if not L:
        break

    for ord in L:
        count[ord] = count.get(ord, 0) + 1

for ord in sorted(count.keys(), key=count.get, reverse=True):
    print(ord, count[ord])
