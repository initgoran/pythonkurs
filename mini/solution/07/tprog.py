from stack import Stack, TomStack

s = Stack()
s.push("Bill")
s.push("Steve")
s.push("Linus")
s.push("Ken")
while True:
    try:
        print(s.pop())
    except TomStack:
        break


