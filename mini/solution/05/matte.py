"""Avancerade matematiska funktioner"""

__version__ = "1.0"
__author__ = "Göran A <initgoran@gmail.com>"
__all__ = "halvera dubblera".split()

def halvera(x):
    "Returnera hälften av argumentet"
    return x / 2

def dubblera(x):
    "Returnera dubbla argumentet"
    return x * 2

def _testfunktion(x):
    assert(halvera(dubblera(x)) == x)

if __name__ == "__main__":
    _testfunktion(2.5)
    _testfunktion(100)
