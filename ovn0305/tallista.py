#!/usr/bin/python3
# coding: utf-8

"""Övning 3.5

* Skriv ett program som läser in heltal (från stdin) tills man matar in
  talet 0. Då ska programmet skriva ut alla talen (utom det sista, 0) på
  varsin rad, sorterade i växande ordning. Om man t.ex. matar in

    5
    -2
    19
    0

ska utskriften bli

    -2
    5
    19

* Uppdatera sedan programmet så att tal som förekommer flera gånger endast
  skrivs ut en gång. Om man t.ex. matar in

    7
    2
    11
    7
    0

ska utskriften bli

    2
    7
    11

"""

