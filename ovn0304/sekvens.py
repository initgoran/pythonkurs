#!/usr/bin/python3
# coding: utf-8

"""Övning 3.4

* Skriv ett program som låter användaren mata in tre värden: start, stop,
  increment. Sekvensen ska skrivas ut på en rad. Därefter ska det hela
  upprepas. T.ex.

  Start: 3
  Stop: 10
  Increment: 2
  3 5 7 9
  Start: 15
  Stop: 35
  Increment: 5
  15 20 25 30

* Avsluta ifall increment <= 0 eller stop < start.

"""

